package bcp.test.adp.utils;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;

public class Utils {
	
	private static WebDriverWait wait;
	
	public static void ejecutarScript(WebDriver driver, String script){
		JavascriptExecutor js = (JavascriptExecutor) driver;  
		js.executeScript(script);
	}
	
	public static void seleccionar(WebDriver driver, WebElement elemento, String texto) throws InterruptedException{  
		esperaPorId(driver,elemento,30);
    	Assert.assertTrue(elemento.isDisplayed());
    	Select destination = new Select (elemento);
    	destination.selectByVisibleText(texto);
    }
	
	public static void esperaPorId (WebDriver driver, WebElement elemento, int segundos){
		wait = new WebDriverWait(driver,segundos);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(elemento.getAttribute("id"))));
	}

}
