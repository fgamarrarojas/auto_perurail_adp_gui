package bcp.test.adp.step;

import net.thucydides.core.annotations.Step;
import org.fluentlenium.core.annotation.Page;

import bcp.test.adp.page.ChooseYourCabin;
import bcp.test.adp.page.ConfirmationAndPayment;
import bcp.test.adp.page.HomePage;
import bcp.test.adp.page.PassengersData;

public class RegisterSteps {
	
	@Page
    HomePage homePage;
	
	@Page
	ChooseYourCabin chooseYourCabin;
	
	@Page
	PassengersData passengersData;
	
	@Page
	ConfirmationAndPayment confirmationAndPayment;
    
	@Step("El usuario ingresa a la pagina principal")
	public void ingresoPeruRails() throws InterruptedException {
		homePage.abrirPaginaPrincipal();
	}
	
	@Step("El usuario selecciona el destino, ruta y fecha en la pagina principal")
	public void seleccionPeruRails(String destino, String ruta) throws InterruptedException {
		homePage.seleccionarDestino(destino);
		homePage.seleccionarRuta(ruta);
		homePage.seleccionarFechaDeSalida();
		homePage.encontrarTicket();
	}
	
	@Step("El usuario selecciona los datos de la cabina")
	public void cabinaPeruRails(int nroCabinas, int nroPasajeros) throws InterruptedException {
		chooseYourCabin.seleccionarNumeroCabinas(nroCabinas,nroPasajeros);
		chooseYourCabin.continuar();
	}
	
	@Step("El usuario introduce sus datos personales")
	public void datosPersonalesPeruRails(String nombre, String apellido, String nacionalidad,String tipoDoc, String nroDoc, String sexo, String correo, String nroTlf, String fechaNacimiento) throws InterruptedException {
		passengersData.llenarDatosCabina1(nombre,  apellido,  nacionalidad, tipoDoc,  nroDoc,  sexo,  correo,  nroTlf,  fechaNacimiento);
		passengersData.continuar();
	}
	
	@Step("El usuario valida la informacion ingresada")
	public void pagoPeruRails(String ruta, String tren, String dia, String cantidad, String total) throws InterruptedException {
		confirmationAndPayment.comprobarCalculo(ruta,  tren,  dia,  cantidad,  total);
	}
}
