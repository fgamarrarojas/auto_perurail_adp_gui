package bcp.test.adp.page;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Assert;

import bcp.test.adp.utils.Utils;

public class HomePage extends PageObject{
	
	@FindBy(id = "btn_search")
    private WebElement findTrainTicketsBtn;
	
	@FindBy(id = "destinoSelect")
    private WebElement destinationSel;
	
	@FindBy(id = "rutaSelect")
    private WebElement routeSel;
	
	@FindBy(id = "countParentsChildren")
    private WebElement adultsChildrenTxt;
	
	@FindBy(id = "salida")
    private WebElement fechaSalidaTxt;
	
	@FindBy(id = "regreso")
    private WebElement fechaRegresoTxt;
	
	WebDriverWait wait;
	
	public void abrirPaginaPrincipal() throws InterruptedException{ 
		getDriver().get("https://www.perurail.com/");
		Utils.esperaPorId(getDriver(),findTrainTicketsBtn,30);
    	Assert.assertTrue(findTrainTicketsBtn.isDisplayed());
    	Utils.ejecutarScript(getDriver(),"window.scroll(0,200)");
    }
	
	public void seleccionarDestino(String destino) throws InterruptedException{ 
		Utils.seleccionar(getDriver(),destinationSel, destino);
    }
	
	public void seleccionarRuta(String ruta) throws InterruptedException{ 
		Utils.seleccionar(getDriver(),routeSel, ruta);
    }
	
	public void seleccionarFechaDeSalida() throws InterruptedException{ 
		fechaSalidaTxt.click();
		Thread.sleep(5000);
		for(int i=0;i<6;i++){
			Utils.ejecutarScript(getDriver(),"document.getElementById(\"ui-datepicker-div\").querySelector(\"div > div > a:nth-child(2)\").click();");
			Thread.sleep(1000);
		}
		Utils.ejecutarScript(getDriver(),"document.getElementById(\"ui-datepicker-div\").querySelector(\"div > table > tbody\").querySelectorAll(\"td[data-handler='selectDay']\").item(4).querySelector(\"td > a\").click();");
		Thread.sleep(1000);
    }
	
	public void encontrarTicket() throws InterruptedException{
		Serenity.takeScreenshot();
		findTrainTicketsBtn.click();
	}
	
	
	
}
