package bcp.test.adp.page;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import bcp.test.adp.utils.Utils;

public class PassengersData extends PageObject{
	
	WebDriverWait wait;
	
	@FindBy(id = "cab-1")
    private WebElement cabina1Div;
	
	@FindBy(id = "txt_nombre[suite][cab1][1]")
    private WebElement cabina1FirstNameTxt;
	
	@FindBy(id = "txt_apellido[suite][cab1][1]")
    private WebElement cabina1LastNameTxt;
	
	@FindBy(id = "sel_nacion[suite][cab1][1]")
    private WebElement cabina1NacionalidadSel;
	
	@FindBy(id = "sel_tpdoc[suite][cab1][1]")
    private WebElement cabina1TipoDocSel;
	
	@FindBy(id = "txt_nroid[suite][cab1][1]")
    private WebElement cabina1NroDocTxt;
	
	@FindBy(id = "sel_sexo[suite][cab1][1]")
    private WebElement cabina1SexoSel;
	
	@FindBy(id = "txt_mail[suite][cab1][1]")
    private WebElement cabina1MailTxt;
	
	@FindBy(id = "txt_mail_conf[suite][cab1][1]")
    private WebElement cabina1MailConfTxt;
	
	@FindBy(id = "txt_telefono[suite][cab1][1]")
    private WebElement cabina1TlfTxt;
	
	@FindBy(id = "txt_fecha_nacimiento[suite][cab1][1]")
    private WebElement cabina1BirthTxt;
	
	@FindBy(id = "chk_ofertas[suite][cab1][1]")
    private WebElement cabina1OfertasCheck;
	
	@FindBy(id = "btnContinuarPas")
    private WebElement continueBtn;
	
	public void llenarDatosCabina1(String nombre, String apellido, String nacionalidad,String tipoDoc, String nroDoc, String sexo, String correo, String nroTlf, String fechaNacimiento) throws InterruptedException{ 
		Utils.esperaPorId(getDriver(),cabina1Div,60);
		cabina1FirstNameTxt.sendKeys(nombre);
		cabina1LastNameTxt.sendKeys(apellido);
		Utils.seleccionar(getDriver(),cabina1NacionalidadSel,nacionalidad);
		Utils.seleccionar(getDriver(),cabina1TipoDocSel,tipoDoc);
		cabina1NroDocTxt.sendKeys(nroDoc);
		Utils.seleccionar(getDriver(),cabina1SexoSel,sexo);
		cabina1MailTxt.sendKeys(correo);
		cabina1MailConfTxt.sendKeys(correo);
		cabina1TlfTxt.sendKeys(nroTlf);
		Utils.ejecutarScript(getDriver(),"document.getElementById(\"txt_fecha_nacimiento[suite][cab1][1]\").readOnly = false");
		cabina1BirthTxt.clear();
		cabina1BirthTxt.sendKeys(fechaNacimiento);
		cabina1OfertasCheck.click();
		Serenity.takeScreenshot();
    }
	
	public void continuar() throws InterruptedException{ 
		Utils.ejecutarScript(getDriver(),"document.getElementById(\"btnContinuarPas\").scrollIntoView()");
		continueBtn.click();
	}
	
}
