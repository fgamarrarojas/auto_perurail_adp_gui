package bcp.test.adp.page;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import bcp.test.adp.utils.Utils;

import org.junit.Assert;

public class ConfirmationAndPayment extends PageObject{
	
	WebDriverWait wait;
	
	@FindBy(id = "forma-pago")
    private WebElement formaPagoForm;
	
	@FindBy(xpath = "/html/body/section/section[1]/div/div[2]/div[2]")
    private WebElement routeTxt;
	
	@FindBy(xpath = "/html/body/section/section[1]/div/div[2]/div[3]/div[1]/div[2]")
    private WebElement trainTxt;
	
	@FindBy(xpath = "/html/body/section/section[1]/div/div[2]/div[3]/div[2]/div[2]")
    private WebElement departureDateTxt;
	
	@FindBy(xpath = "/html/body/section/section[1]/div/div[2]/div[4]/div[2]/span[1]")
    private WebElement nroPersonasTxt;
	
	@FindBy(xpath = "/html/body/section/section[1]/div/div[2]/div[4]/div[2]/span[2]")
    private WebElement totalCobroTxt;
	
	public void comprobarCalculo(String ruta, String tren, String dia, String cantidad, String total) throws InterruptedException{ 
		Utils.esperaPorId(getDriver(),formaPagoForm,60);
		Utils.ejecutarScript(getDriver(),"document.getElementById(\"compra\").querySelector(\"section > a\").click()");
		System.out.println("===========================================================");
		Assert.assertEquals(ruta, routeTxt.getText());
		System.out.println(routeTxt.getText());
		Assert.assertEquals(tren, trainTxt.getText());
		System.out.println(trainTxt.getText());
		Assert.assertEquals(dia, departureDateTxt.getText());
		System.out.println(departureDateTxt.getText());
		Assert.assertEquals(cantidad, nroPersonasTxt.getText());
		System.out.println(nroPersonasTxt.getText());
		Assert.assertEquals(total, totalCobroTxt.getText());
		System.out.println(totalCobroTxt.getText());
		System.out.println("===========================================================");
		Serenity.takeScreenshot();
    }
	
}
