package bcp.test.adp.page;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import bcp.test.adp.utils.Utils;

public class ChooseYourCabin extends PageObject{
	
	WebDriverWait wait;
	
	@FindBy(id = "btn_search_bae")
    private WebElement findTicketsBtn;
	
	@FindBy(id = "continuar_bae")
    private WebElement continueBtn;
	
	public void seleccionarNumeroCabinas(int nroCabinas, int nroPasajeros) throws InterruptedException{ 
		Utils.esperaPorId(getDriver(),findTicketsBtn,60);
		Utils.ejecutarScript(getDriver(),"document.querySelector(\"select[name='selectRooms[suite]']\").scrollIntoView()");
		Thread.sleep(1000);
		Utils.ejecutarScript(getDriver(),"document.querySelector(\"select[name='selectRooms[suite]']\").value = '"+nroCabinas+"'");
		Thread.sleep(1000);
		Utils.ejecutarScript(getDriver(),"document.querySelector(\"select[name='selectRooms[suite]']\").dispatchEvent(new Event('change'))");
		Thread.sleep(1000);
		Utils.ejecutarScript(getDriver(),"document.querySelector(\"select[name='selectRooms[suite][cabinas][cab1][adult]']\").getElementsByTagName('option')["+nroPasajeros+"].selected = 'selected'");
		Thread.sleep(1000);
		Utils.ejecutarScript(getDriver(),"document.querySelector(\"select[name='selectRooms[suite][cabinas][cab1][adult]']\").dispatchEvent(new Event('change'))");
		Thread.sleep(1000);
		Serenity.takeScreenshot();
    }
	
	public void continuar() throws InterruptedException{ 
		Utils.ejecutarScript(getDriver(),"document.getElementById(\"continuar_bae\").scrollIntoView()");
		continueBtn.click();
	}
	
}
